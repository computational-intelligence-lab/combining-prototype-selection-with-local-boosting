from sklearn.ensemble import AdaBoostClassifier
from sklearn.datasets import load_digits as data
from sklearn.cross_validation import cross_val_score as cv

from prototype_selected_local_boosting import PSLBClassifier

X, y = data().data, data().target

pslb = PSLBClassifier(random_state=1, verbose=True)
ada_boost = AdaBoostClassifier(random_state=1)
print 'Cross-validation for PSLBClassifier started...'
pslb_score = cv(pslb, X, y, cv=10).mean()
print 'Cross-validation for AdaBoostClassifier started...'
adaboost_score = cv(ada_boost, X, y, cv=10).mean()
print 'PSLBClassifier accuracy: {}'.format(pslb_score)
print 'AdaBoostClassifier accuracy: {}'.format(adaboost_score)
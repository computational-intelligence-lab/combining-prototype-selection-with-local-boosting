#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division

import warnings

import numpy as np
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.utils.validation import check_X_y


class PSLBClassifier(AdaBoostClassifier):

    """
    A prototype selected local boost classifier.

    A prototype selected local boost classifier is a 
    meta-estimator that makes prototype selection at the 
    training phase. In the prediction phase for each test 
    instance chooses the k closest instances and builds a 
    local model for each test instance by fitting a 
    classifier on the choosen instances and then fits 
    additional copies of the classifier on the same 
    choosen instances but where the weights of incorrectly 
    classified instances are adjusted such that subsequent 
    classifiers focus more on difficult cases.



    Parameters
    ----------
    base_estimator : object, optional (default=DecisionTreeClassifier)
        The base estimator from which the boosted ensemble is built.
        Support for sample weighting is required, as well as proper 'classes_'
        and 'n_classes_' attributes.

    n_estimators : integer, optional (default=50)
        The maximum number of estimators at which boosting is terminated.
        In case of perfect fit, the learning procedure is stopped early.

    learning_rate : float, optional (default=1.)
        Learning rate shrinks the contribution of each classifier by
        ''learning_rate''. There is a trade-off between ''learning_rate'' and
        ''n_estimators''.

    algorithm : {'SAMME', 'SAMME.R'}, optional (default='SAMME.R')
        If 'SAMME.R' then use the SAMME.R real boosting algorithm.
        ''base_estimator'' must support calculation of class probabilities.
        If 'SAMME' then use the SAMME discrete boosting algorithm.
        The SAMME.R algorithm typically converges faster than SAMME,
        achieving a lower test error with fewer boosting iterations.

    random_state : int, RandomState instance or None, optional (default=None)
        If int, random_state is the seed used by the random number generator;
        If RandomState instance, random_state is the random number generator;
        If None, the random number generator is the RandomState instance used
        by 'np.random'.

    n_local_neighbors : int, optional (default = 50)
        Number of neighbors to use for local models.

    n_ps_neighbors :  int, optional (default = 5)
        Number of neighbors to use for prototype selection

    metric : string or DistanceMetric object (default = 'euclidean')
        the distance metric to use for the tree. The default metric is euclidean.

    verbose : boolean, optional
        Sets the verbosity amount.
        """

    def __init__(self,
                 base_estimator=None,
                 n_estimators=25,
                 learning_rate=1.0,
                 algorithm='SAMME.R',
                 random_state=None,
                 n_local_neighbors=50,
                 n_ps_neighbors=5,
                 metric='euclidean',
                 verbose=False):
        self.n_local_neighbors = n_local_neighbors
        self.n_ps_neighbors = n_ps_neighbors
        self.metric = metric
        self.verbose = verbose
        return super(PSLBClassifier, self).__init__(base_estimator,
                                                    n_estimators,
                                                    learning_rate,
                                                    algorithm,
                                                    random_state)

    def _select_prototypes(self):
        self.knn_ = KNeighborsClassifier(
            n_neighbors=self.n_ps_neighbors, metric=self.metric)
        if self.verbose:
            print('Prototype selection started...')
        self.knn_.fit(self.X_fit_, self.y_fit_)
        p = self.knn_.predict(self.X_fit_)

        selection = p == self.y_fit_

        self.X_ps_ = self.X_fit_[selection]
        self.y_ps_ = self.y_fit_[selection]
        if self.verbose:
            n_removed = self.y_fit_.shape[0] - self.y_ps_.shape[0]
            print('%d instances removed' % n_removed)
            print('Prototype selection ended')
        return self.X_ps_, self.y_ps_

    def fit(self, X, y, sample_weight=None):

        X, y = check_X_y(X, y)

        self.X_fit_ = np.copy(X)
        self.y_fit_ = np.copy(y)

        self._select_prototypes()
        local_params = {"n_neighbors": self. n_local_neighbors}
        self.knn_.set_params(**local_params)
        self.knn_.fit(self.X_ps_, self.y_ps_)

        self.classes_ = np.unique(y)
        self.n_classes_ = len(self.classes_)

        return self

    def predict(self, X):
        X = np.atleast_2d(X)
        predictions = np.apply_along_axis(self._fit_predict, 1, X)
        return predictions.ravel()

    def _get_nearest_neighbors(self, X):
        if self.n_local_neighbors > len(self.y_ps_):
            warnings.warn("The number of the closest neighbors is lower than the"
                          " n_local_neighbors %s < %s" % (len(self.y_ps_),
                                                          self.n_local_neighbors))
            n_nn = len(self.y_ps_)
        else:
            n_nn = self.n_local_neighbors
        return self.knn_.kneighbors(X, n_nn)[1][0]

    def _fit_predict(self, x):
        x = x.reshape(1, -1)
        # Get the n local nearest neighborrs
        neighbors = self._get_nearest_neighbors(x)

        # If all the neighbors have the same class then
        classes_ = np.unique(self.y_ps_[neighbors])
        if len(np.unique(self.y_ps_[neighbors])) == 1:
            prediction = classes_
        else:
            super(PSLBClassifier, self).fit(
                self.X_ps_[neighbors], self.y_ps_[neighbors])
            prediction = super(PSLBClassifier, self).predict(x)
        return prediction

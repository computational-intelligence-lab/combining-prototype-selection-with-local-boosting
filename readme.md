Ch.K. Aridas, S.B. Kotsiantis, M.N. Vrahatis  
**Combining Prototype Selection with Local Boosting**  
12th IFIP International Conference on Artificial Intelligence Applications and Innovations (AIAI 2016), Springer  
https://doi.org/10.1007/978-3-319-44944-9_9

Please cite this paper when using this code for your research.